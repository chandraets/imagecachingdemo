//
//  Post.swift
//  ImageCachingDemo
//
//  Created by Puja Bhagat on 20/12/2021.
//

import Foundation

struct ProfileImage: Codable {
    let small: String
    let medium: String
}

struct User: Codable {
    let profile_image: ProfileImage
}

struct Urls: Codable {
    let regular: String
}

struct Post: Codable {
    let id: String
    let description: String?
    let user: User
    let urls: Urls
}
