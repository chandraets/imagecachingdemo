//
//  ImageCollectionViewCell.swift
//  ImageCachingDemo
//
//  Created by Puja Bhagat on 20/12/2021.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var badgeImageView: UIImageView!
    
    var representedIdentifier: String = ""
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    
    var badge: UIImage? {
        didSet{
            badgeImageView.image = badge
        }
    }
    
    //MARK:- PRIVATE METHODS
    
    //MARK:- PUBLIC METHODS
    
    //MARK:- ACTION METHODS
    
    //MARK:- OVERRIDE METHODS
    
    
    //MARK:- DELEGATE METHODS
    
    
    
    
}
