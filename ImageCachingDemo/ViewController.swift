//
//  ViewController.swift
//  ImageCachingDemo
//
//  Created by Puja Bhagat on 20/12/2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let networker = NetworkHandler.shared
    
    var posts: [Post] = []
    
    //MARK:- PRIVATE METHODS
    
    //MARK:- PUBLIC METHODS
    
    
    //MARK:- ACTION METHODS
    
    //MARK:- OVERRIDE METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        networker.posts(query: "puppies") { [weak self] (posts, error) in
            if let error = error {
                debugPrint("error")
                return
            }
            
            self?.posts = posts!
            
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
            
        }
    }
    
    //MARK:- DELEGATE METHODS
    
}

extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        let post = self.posts[indexPath.item]
        cell.title = post.description
        cell.image = nil
        cell.badge = nil
        let representedIdentifier = post.id
        cell.representedIdentifier = post.id
        
        func image(data: Data?) -> UIImage? {
            if let data = data {
                return UIImage(data: data)
            }
            return UIImage(systemName: "picture")
        }
        
        networker.image(post: post) { [unowned self] (data, error) in
            if let data = data {
                let image = image(data: data)
                DispatchQueue.main.async {
                    if cell.representedIdentifier == representedIdentifier{
                        cell.image = image
                    }
                }
            }
        }
        
        networker.profileImage(post: post) { [unowned self] (data, error) in
            if let data = data {
                let image = image(data: data)
                DispatchQueue.main.async {
                    if cell.representedIdentifier == representedIdentifier{
                        cell.badge = image
                    }
                }
            }
        }
        return cell
    }
}

