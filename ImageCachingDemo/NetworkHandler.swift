//
//  NetworkHandler.swift
//  ImageCachingDemo
//
//  Created by Puja Bhagat on 20/12/2021.
//

import Foundation

enum NetworkError: Error {
    case badUrl
    case badResponse
    case badStatusCode(Int)
    case badData
}

fileprivate struct APIResponse: Codable {
    let results: [Post]
}

class NetworkHandler {
    static var shared = NetworkHandler()
    private let urlSession: URLSession
    private var images = NSCache<NSString, NSData>()
    
    init() {
        //
        let config = URLSessionConfiguration.default
        urlSession = URLSession(configuration: config)
    }
    
    private func components() -> URLComponents {
        var comp = URLComponents()
        comp.scheme = "https"
        comp.host = "api.unsplash.com"
        return comp
    }
    
    private func urlRequest(url: URL) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.addValue("Client-ID \(accesKey)", forHTTPHeaderField: "Authorization")
        return urlRequest
    }
    
    func posts(query:String, completion: @escaping (([Post]?, Error?) -> (Void))){
        let path = "/search/photos"
        var comp = components()
        comp.path = path
        
        comp.queryItems = [
            URLQueryItem(name: "query", value: query)
        ]
        
        let urlRequest = urlRequest(url: comp.url!)
        
        let task = urlSession.dataTask(with: urlRequest) { (data: Data?, urlResponse: URLResponse?, error: Error?) in
            if let error = error {
                completion(nil, error)
                return
            }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse  else {
                completion(nil, NetworkError.badResponse)
                return
            }
            
            guard (200...299).contains(httpResponse.statusCode) else {
                completion(nil, NetworkError.badStatusCode(httpResponse.statusCode))
                return
            }
            
            guard let data = data else {
                completion(nil, NetworkError.badData)
                return
            }
            
            do {
                let response = try JSONDecoder().decode(APIResponse.self, from: data)
                debugPrint("Response: \(response)")
                completion(response.results, nil)
            } catch {
                completion(nil, error)
            }
        }
        
        task.resume()
    }
    
    func image(post: Post, completion: @escaping((Data?, Error?) -> Void)){
        guard let url = URL(string: post.urls.regular) else {
            debugPrint("Invalid Url: \(post.urls.regular)")
            completion(nil, NetworkError.badUrl)
            return
        }
        downloadImage(url: url, completion: completion)
    }
    
    func profileImage(post: Post, completion: @escaping((Data?, Error?) -> Void)){
        guard let url = URL(string: post.user.profile_image.medium) else {
            debugPrint("Invalid Url: \(post.urls.regular)")
            completion(nil, NetworkError.badUrl)
            return
        }
        downloadImage(url: url, completion: completion)
    }

    func downloadImage(url: URL, completion: @escaping((Data?, Error?) -> Void)){
        if let imageData = images.object(forKey: url.absoluteString as NSString) {
            debugPrint("Using cached images")
            completion(imageData as Data, nil)
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        let task = urlSession.downloadTask(with: urlRequest) { [unowned self] (localUrl:URL?, urlResonse: URLResponse?, error: Error?) in
            
            if let error = error {
                completion(nil, error)
                return
            }
            
            guard let httpRespose = urlResonse as? HTTPURLResponse else {
                completion(nil, NetworkError.badResponse)
                return
            }
            
            guard(200...299).contains(httpRespose.statusCode) else {
                completion(nil, NetworkError.badStatusCode(httpRespose.statusCode))
                return
            }
            
            guard let localUrl = localUrl else {
                completion(nil, NetworkError.badData)
                return
            }
            
            do {
                let data = try Data(contentsOf: localUrl)
                self.images.setObject(data as NSData, forKey: url.absoluteString as NSString)
                completion(data, nil)
            }catch{
                completion(nil, error)
            }
        }
        task.resume()
    }
}
